var o = {};

// External URL
o.url = 'http://localhost:9999/';

// Allows use URL for page requests [ http://.../#pageName/?var1=val1&...&varN=valN ]
o.purl = true;

module.exports = o;
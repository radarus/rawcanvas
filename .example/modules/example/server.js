/**
 * Module server script example
 *
 * Available by URIs:
 *  http(s)://{server}/modules/{module name}
 *  http(s)://{server}/modules/{module name}/
 *  http(s)://{server}/modules/{module name}/server.js
 * 
 * For example:
 *  http(s)://{server}/modules/example
 *  http(s)://{server}/modules/example/
 *  http(s)://{server}/modules/example/server.js
 *
 * @param req
 * @param res
 */
module.exports = function example (req, res)
 {
  res.send(req.params.name);
 };
var o = {};

// External URL
o.url = 'http://localhost:3000/';

// Allows use URL for page requests [ http://.../#pageName/?var1=val1&...&varN=valN ]
o.purl = false;

module.exports = o;
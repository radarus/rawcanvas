var config = {};

config.generic = require("./generic.js");
config.server = require("./server.js");
config.client = require("./client.js");

module.exports = config;
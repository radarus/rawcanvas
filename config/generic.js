var o = {};

o.debug = {};
o.debug.status = true;
/**
 * Indicates that HTML & CSS code will minified on fly.
 * It allows to see how it will on production, but without static bundle creation.
 * But also this option has negative affect to performance for debug mode only.
 * NOTE: When HTML code is minified the spaces around of HTML tags is removed, 
 * that have affect UI
 */
o.debug.minify = false;

o.environment = {};

o.environment.modes = {};
o.environment.modes.MASTER = 'master';
o.environment.modes.SLAVE = 'slave';

/**
 * Available modes:
 *
 *  o.environment.modes.MASTER:
 *   Self-contained mode - that means the engine uses
 *   pages and layouts which was predefined by configuration
 *
 *  o.environment.modes.SLAVE:
 *   Slave mode - that means the engine will driven by other
 *   environment. The engine provides only module logic for this case
 *
 * SLAVE mode usage example:
 * 
 * [body]
 * [script type="text/javascript" src="http(s)://path.to.slave.app.host/bootstrap.js"][/script]
 * 
 * [script type="text/javascript"]
 *  $R$.ready(function(){
 *
 *   console.log($R$);
 *
 *   var moduleExample = $R$.module.make(null, 'example', {});
 *   document.body.appendChild(moduleExample.options.DOM);
 *   moduleExample.show({a:1});
 *
 *  });
 * [/script]
 *
 */
o.environment.mode = o.environment.modes.SLAVE;

o.bootstrap = {};
o.bootstrap.progress = true; // Enable/disable bootstrap progress bar

o.modules = {};
o.modules.url = '/modules';

o.layouts = {};
o.layouts.url = '/layouts';

o.page = {};
o.page["default"] = {};
o.page["default"].name = 'index';

o.layout = {};
o.layout["default"] = {};
o.layout["default"].name = 'index';

module.exports = o;
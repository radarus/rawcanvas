var o = {};

o.port = 3000;

/**
 * Indicates the COM-resources that must be compiled to static and minified 
 * on production (when debug mode is disabled)
 * 
 * WARNING: This settings affects to system performance.
 *          It is highly recommended to define as enabled for all COM-resources 
 */
o.buildLayouts = true;
o.buildModules = true;

o.framework = {};
o.framework.dir = __dirname + '/..';

o.application = {};
o.application.dir = __dirname + '/..';

/**
 * [optional] Path to the file that provides additional server routes.
 * Recommended path to the file is "app/server/routes.js"
 */
o.application.router = null;

/**
 * [optional] Path to the file that can process command line arguments
 * Recommended path to file is "app/server/args.js"
 */
o.application.argsProcessor = null;

o.modules = {};
o.modules.dir = o.application.dir + '/modules';

o.layouts = {};
o.layouts.dir = o.application.dir + '/layouts';

o.statics = {};

o.application.build = [];
o.framework.build = [];
o.framework.build.push(o.framework.dir + '/core/js/bootstrap.js');
o.framework.build.push(o.framework.dir + '/core/js/generic/com.js');

module.exports = o;

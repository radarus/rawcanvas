(new function $R (window, document, location, Array, Number, String) {
 var $R = this;

 var DEBUG = (Boolean(/*[variable name="config.generic.debug.status"]*/) == true) ? true : false;

 if (DEBUG)
  {
   window.$R = $R;
  }

 var NUMBER = 'number', STRING = 'string', OBJECT = 'object', FUNCTION = 'function', UNDEFINED = 'undefined', BOOLEAN = 'boolean'; // For typeof checking

 var makeCSSNodeByString = function(str)
  {
   var o = document.createElement("style");
   o.type = "text/css";
   if(o.styleSheet) o.styleSheet.cssText = str;
   else o.appendChild(document.createTextNode(str));
   return o;
  };

 var progressBarMarkup = {
   html: String(/*[require src="sources/progressbar/index.html" format="JSON" default=""]*/),
   css: String(/*[require src="sources/progressbar/style.css" format="JSON" default=""]*/)
 };

 function initBootstrapStyles ()
  {
   var node;
   node = makeCSSNodeByString(/*[require src="core/css/bootstrap.css" namespace="FRAMEWORK" format="JSON"]*/);
   document.querySelector('head').appendChild(node);

   node = makeCSSNodeByString(progressBarMarkup.css);
   document.querySelector('head').appendChild(node);
  }
 initBootstrapStyles();

 /*[require src="core/js/generic/progressbar.js" namespace="FRAMEWORK"]*/

 // Create and show boot progress
 $R.bootProgress = new $R.progressBar();
 $R.bootProgress.setMessage('Initial loading...')
                .setCompleteMessage('Loading complete');

 if (Boolean(/*[variable name="config.generic.bootstrap.progress"]*/))
  {
   $R.bootProgress.show();
  }

 /*[require src="core/js/generic/log.js" namespace="FRAMEWORK"]*/
 /*[require src="core/js/generic/error.js" namespace="FRAMEWORK"]*/
 /*[require src="core/js/generic/includers.js" namespace="FRAMEWORK"]*/

 // Ready ---------------------------------------------------------------------

 var _ready = [], _isReady = false;

 function ready (fn)
  {
   if (typeof fn != FUNCTION) return;
   if (_isReady) fn();
   else _ready.push(fn);
  }

 function _procReady ()
  {
   _isReady = true;
   for (var i=0; i<_ready.length; i++) _ready[i]();
   _ready = [];
  }

 // Bootstrap -----------------------------------------------------------------
 // $R.bootProgress.setStepCallback(function(action, sign, amount, signObj, total, passed, signs, o){console.log(action, sign, amount, signObj, total, passed, signs);});
 function __construct ()
  {
   // Boot progress steps limit definition
   $R.bootProgress
     .pushStep('initConfig')
     .pushStep('initTools')
     .pushStep('initCOM')
     .pushStep('initStorage')
     .pushStep('onEventLoad')
   ;
   $R.bootProgress.setMessage('Retrieving system modules...');

   initConfig(); //NOTE: It should be synchronius only!!!

   if ( isSlaveMode() )
    {
     window.$R$ = new function $R$ () {} ();
     window.$R$.ready = ready;
    }

   var onEventLoad = function onEventLoad (){

    !new $R.eventFactory($R);

    if ( isSlaveMode() )
     {
      window.$R$.addEventListener = $R.addEventListener;
      window.$R$.removeEventListener = $R.removeEventListener;
      window.$R$.dispatchEvent = $R.dispatchEvent;
     }

    $R.dispatchEvent ('initConfig');

    $R.bootProgress.setCompleteCallback(function(){
     // System loading complete
     $R.dispatchEvent ('bootComplete');

     if ( isMasterMode() )
      {
       $R.addEventListener('initCOMMap:ready:once', function(){
        if ($R.config.client.purl == true) initRouter();
        else renderInitialPage();        
       });
      }

     if ( isSlaveMode() )
      {
       window.$R$.module = $R.module;
       _procReady();
      }
    });

    initTools();
    initCOM();

    initStorage();
    initBrowser();
    initCOMDefinitions();
    procOnBoot();

    $R.bootProgress.doStep('onEventLoad').setMessage('System event provider loaded');
   };
   /*[require src="core/js/generic/event.js" namespace="FRAMEWORK"]*/
   onEventLoad();
  };

 function initConfig ()
  {
   $R.config = function(){return /*[variable name="config"]*/;}();
   $R.url = function url (path)
    {
     var a = $R.config.client.url;
     var b = (typeof path == STRING) ? path : '';
     var c = a + ( (a.substr(a.length-1,1) == '/' && b.substr(0,1) == '/') ? b.substr(1) : b );
     return c;
    };
   $R.bootProgress.doStep('initConfig').setMessage('System config loaded');
  }

 function initTools ()
  {
   var onToolsLoad = function onToolsLoad (){
    $R.tools.makeCSSNodeByString = makeCSSNodeByString;
    $R.dispatchEvent ('initTools');
    $R.bootProgress.doStep('initTools').setMessage('System tools loaded');
   };
   /*[require src="core/js/generic/tools.js" namespace="FRAMEWORK"]*/
   onToolsLoad();
  }

 function initCOM ()
  {
   var onCOMLoad = function onCOMLoad () {
    $R.bootProgress.doStep('initCOM').setMessage('COM logic loaded');
   };
   /*[require src="core/js/generic/com.js" namespace="FRAMEWORK"]*/
   onCOMLoad();
  }

 function initStorage ()
  {
   var onStorageLoad = function onStorageLoad () {
    $R.dispatchEvent ('initStorage');
    $R.bootProgress.doStep('initStorage').setMessage('System storage init process complete');
   };
   /*[require src="core/js/generic/storage.js" namespace="FRAMEWORK"]*/
   onStorageLoad();
  };

 function isMasterMode ()
  {
   var env = $R.config.generic.environment;
   return (env.mode == env.modes.MASTER);
  };
 $R.isMasterMode = isMasterMode;

 function isSlaveMode ()
  {
   var env = $R.config.generic.environment;
   return (env.mode == env.modes.SLAVE);
  };
 $R.isSlaveMode = isSlaveMode;

 function initRouter ()
  {
   /*[require src="core/js/generic/router.js" namespace="FRAMEWORK"]*/
  }

 function renderInitialPage ()
  {
   if ($R.page.defined()[$R.config.generic.page['default'].name] == null)
    {
     (new $R.progressBar).pushStep('renderInitialPage').show()
     .setMessage ('The application did not configured yet for master mode.<br />The reason - default page did not described.');
     return;
    }
   $R.page.get($R.config.generic.page['default'].name).show();
  }

 var _onBoot = [];

 function procOnBoot ()
  {
   for (var i=0; i<_onBoot.length; i++)
    {
     try
      {
       _onBoot[i]();
       $R.bootProgress.doStep('onBoot');
      }
     catch(e)
      {
       $R.error($R.error.core, 'Javascript exception in boot process', null, {
        message: e.message,
        stack: (typeof e.stack != UNDEFINED) ? e.stack : e,
        source: _onBoot[i]
       });
       throw e;
      };
    }
   _onBoot = [];
  }

 function onBoot (fn)
  {
   $R.bootProgress.pushStep('onBoot');
   if(typeof fn == FUNCTION)
    {
     _onBoot.push(fn);
    }
   else
    {
     $R.error($R.error.core, 'Boot process definition error', null, {
      message: 'Boot process is not a function',
      source: fn
     });
     throw 'Boot process is not a function';
    }
  }

 function initBrowser ()
  {
   onBoot(function(){
    var node = document.getElementsByTagName('html')[0];
    var b = $R.tools.browser;
    $R.tools.addClass(node, [b.isMobile() ? 'mobile' : '', b.engine(), b.os()]);    
   });
  }

 function initCOMDefinitions ()
  {
   onBoot(function(){
    $R.addEventListener('initCOM:ready:once', function(){
     /*[require src="config/map.js" default=""]*/
     $R.dispatchEvent('initCOMMap');
    });
   });
  }

 /*[require src="js/inc.js" default="'done';"]*/

 __construct();
} (window, document, location, Array, Number, String));
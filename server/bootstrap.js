/**
 * Available commandline arguments
 * port => node index.js port=2000
 */

var helper = require('./helpers.js');
var config_js = __dirname + '/../config/config.js';

function init (config)
 {
  // The config will be extended if it's needed.
  // Actually it can be used for the case when one instance of framework will used for several instances of app
  if (config != null && typeof config == 'object')
   {
    helper.extend(require(config_js), config);
    helper.extend(config, require(config_js));
   }

  bootstrap();
 }

function bootstrap ()
 {
  var express = require('express');
  var app = express();
  var config = require(config_js);
  global.__arguments = helper.parseArguments(process.argv.splice(2)); // Command line arguments

  // Define common statics
  config.server.statics['/core'] = config.server.framework.dir + '/core';
  config.server.statics[config.generic.modules.url] = config.server.modules.dir;
  config.server.statics[config.generic.layouts.url] = config.server.layouts.dir;  

  var builder = require(__dirname + '/builder.js');
  var bundle = require(__dirname + '/bundle.js');
  if (!config.generic.debug.status) builder.run();
  if (__arguments.bundle == true)
   {
    if (!config.generic.debug.status) bundle.make();
    else console.log ('\nThe bundle cannot be created while DEBUG mode enabled by application configuration.\nPlease turn of DEBUG mode (config.generic.debug.status = false)\n');
    return;
   }
  
  app.configure('development', function()
   {
    app.use(express.logger('dev'));
   });

  app.configure(function()
   {
    app.use(express.compress());
    //app.use(express.favicon());
    app.use(express.bodyParser());
    app.use(express.cookieParser({path: '/', maxAge: 14400000}));

    app.use(function(req, res, next){
      res.type(helper.getContentTypeByFilename(req.url, 'html'));
      next();
    });

    app.use(helper.allowCrossDomain);

    app.use(express.methodOverride());
    app.use(app.router);

    // Preparing common statics
    for (var url in config.server.statics)
     {
      app.use(url, express.static(config.server.statics[url]));
     }
   });

  // init common routes
  require(__dirname + '/routes.js')(express, app);

  // init additional routes if it`s needed
  if (typeof config.server.application.router == 'string')
   {
    require(config.server.application.router)(express, app);
   }

  var port = Number( __arguments.port || config.server.port );
  app.listen(port);
  console.log('The server started at ' + port + ' port');

  // init additional command line arguments processor if it`s needed
  if (typeof config.server.application.argsProcessor == 'string')
   {
    require(config.server.application.argsProcessor)(__arguments);
   }

 }

module.exports = init;
module.exports.run = run;
module.exports.path = path;
module.exports.version = version;
module.exports.list = list;
module.exports.dir = dir;

var FS = require('fs');
var PATH = require('path');

var config = require(__dirname + '/../config/config.js');
var minify = require(__dirname + '/minify.js');

var STATUS = !config.generic.debug.status;
var FILE = [];
var DIR = config.server.application.dir + '/.build';

function run ()
 {
  if (!STATUS) return;
  prepare();
  for (var i=0; i<FILE.length; i++)
   {
    var source = FILE[i];
    var destination = DIR + '/' + convert(FILE[i]);
    make(source, destination);
   }
 }

function convert (filepath)
 {
  return PATH.normalize(filepath).replace(/[\/\\\:\.\!]/g, '_');
 }

function path (filepath)
 {
  if (!STATUS) return filepath;
  var path = DIR + '/' + convert(filepath);
  return FS.existsSync(path) ? path : filepath;
 }

function version ()
 {
  return new Date;
 }

function list ()
 {
  return FILE;
 }

function dir ()
 {
  return DIR;
 }

function make (source, destination)
 {
  var s = FS.readFileSync(source,'utf-8');
  s = require(__dirname + '/tag.generic.js')(s);
  
  if (source.match(/\.(html|htm)$/i)) s = minify.html(s);
  else if (source.match(/\.(css)$/i)) s = minify.css(s);
  else if (source.match(/\.(js)$/i)) s = minify.js(s);
  
  var dir = PATH.dirname(destination);
  if (!FS.existsSync(dir)) FS.mkdirSync(dir, 0777);

  FS.writeFileSync(destination, s, 'utf-8');
 }

function prepare ()
 {
  var modules = [], layouts = [];
  if (config.server.buildModules) modules = getCOMFilesList(config.server.modules.dir);
  if (config.server.buildLayouts) layouts = getCOMFilesList(config.server.layouts.dir);
  var o = [].concat(
    config.server.application.build || [], 
    config.server.framework.build || [],
    modules,
    layouts
  );
  for (var i=0; i<o.length; i++)
   {
    var f = PATH.normalize(o[i]);
    if (FILE.indexOf(f) < 0) FILE.push(f);
   }
 }

function getCOMFilesList (dir)
 {
  var o = [];

  function process (dirname)
   {
    var path = dir + '/' + dirname + '/';
    if ( FS.existsSync(path + 'client.js') ) o.push(path + 'client.js');
    if ( FS.existsSync(path + 'style.css') ) o.push(path + 'style.css');
    if ( FS.existsSync(path + 'index.html') ) o.push(path + 'index.html');
   }

  var dirs = FS.readdirSync(dir);
  for (var i=0; i<dirs.length; i++) process(dirs[i]);
  return o;
 }
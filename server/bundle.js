module.exports.status = status;
module.exports.config = config;
module.exports.make = make;
module.exports.chk = chk;

var CONFIG = require(__dirname + '/../config/config.js');
var HELPER = require(__dirname + '/helpers.js');
var BUILDER = require(__dirname + '/builder.js');

var FS = require('fs');
var PATH = require('path');

function status ()
 {
  return (__arguments.bundle == true && !CONFIG.generic.debug.status);
 }

var _config;
function config ()
 {
  if (!status()) return false;
  if (_config != null) return _config;
  var o;
  try { o = new Function('return '+FS.readFileSync(CONFIG.server.application.dir + '/bundle.json','utf-8'))(); }
  catch (e) {};
  o = (typeof o == 'object' && o != null) ? o : {};
  for (var i in __arguments) if (i != 'bundle') o[i] = __arguments[i];
  o.source = convert(CONFIG.server.application.dir, true);

  if (typeof o.destination != 'string')
   {
    console.log('Destination path is not defined');
    process.exit(0);
   }
  o.destination = normalizeDestinationPath(o.destination);

  o.clean = (typeof o.clean != 'object' || o.clean == null || typeof o.clean.length != 'number') ? [] : convert(o.clean);
  o.ignore = (typeof o.ignore != 'object' || o.ignore == null || typeof o.ignore.length != 'number') ? [] : convert(o.ignore);

  if (o.cleanCOM == true)
   {
    var com = {'layouts': CONFIG.server.layouts.dir, 'modules':CONFIG.server.modules.dir};
    for (var i in com)
     {
      var sub = FS.readdirSync(convert(com[i]));
      for (var j in sub) o.clean.push(convert(i + '/' + sub[j] + '/index.html'), convert(i + '/' + sub[j] + '/style.css'));
     }
   }
  _config = o;
  return _config;
 }

function make ()
 {
  if (!status()) return false;
  var ncp = ncpInstance();

  var source = getParentPathDescriptor(CONFIG.server.application.dir);

  function update ()
   {
    var copy = [
      {from: convert(CONFIG.server.framework.dir+'/index.html'), to: convert(config().destination+'/index.html')},
      {from: BUILDER.path(convert(CONFIG.server.framework.dir+'/core/js/bootstrap.js')), to: convert(config().destination+'/bootstrap.js')},
    ];
    var file = convert(BUILDER.list());

    for (var i=0; i<file.length; i++)
     {
      if (file[i].match(source.exp))
       {
        var relative = file[i].replace(source.exp, '');
        if (!isIgnore(relative) && !isSkip(relative))
         {
          copy.push({ from: BUILDER.path(file[i]), to: convert(config().destination + '/' + relative) });
         }
       }
     }

    //console.log(copy);
    var count = copy.length;
    var callback = function(err)
     {
      if (err) 
       {
        console.error(err);
        process.exit(0);
       }
      count--;
      //console.log(count)
      if (count <= 0)
       {
        console.log('\nThe bundle creating process completed.\n\nSource      : "'+config().source+'"\nDestination : "'+config().destination+'"\n');
        process.exit(0);
       }
     };
    for (var i=0; i<copy.length; i++)
     {
      HELPER.fileCopy(copy[i].from, copy[i].to, callback);
     }
   }

  var includes = [];
  for (var i in CONFIG.server.statics) includes.push(HELPER.pregQuote(convert(CONFIG.server.statics[i], true)));
  includes = new RegExp('^(' + includes.join('|') + ')');

  var options = {
    filter: function (path) {
     var status = false;
     var path = convert(path);
     if (path == source.normalized || path.match(source.exp))
      {
       var relative = path.replace(source.exp, '');
       var isServerJS = !!(path.match(/(server\.js)$/i)); // TODO: add directory separator to regexp before filename 
       status = !!(!isServerJS && !isIgnore(relative) && !isSkip(relative) && (path == source.normalized || path.match(includes)));
      }
     //console.log(status, path);
     return status;
    }
  };

  ncp(source.normalized, config().destination, options, function (err) {
   if (err) 
    {
     console.error(err);
     process.exit(0);
    }
   update();
  });
 }

function chk (path)
 {
  if (!status()) return true;
  var p = convert(path, true);
  if (isIgnore(p)) return false;
  p = getRelativeToParentExistsPath(convert(CONFIG.server.application.dir), p);
  if (p && isIgnore(p)) return false;
  return true;
 }

function isIgnore (path)
 {
  if (!status()) return false;
  return isPathExists(config().ignore, path);
 }

function isSkip (path)
 {
  if (!status()) return false;
  return isPathExists(config().clean, path);
 }

function getParentPathDescriptor (parentPath)
 {
  var n = convert(parentPath, true);
  var re = new RegExp('^('+HELPER.pregQuote(n)+'['+HELPER.pregQuote('/\\')+'])');
  return {normalized:n, exp:re};
 }

function getRelativeToParentExistsPath (parentPath, fullPath)
 {
  var parentDescriptor = getParentPathDescriptor(parentPath);
  var path = convert(fullPath, true);
  if (path.match(parentDescriptor.exp)) return path.replace(parentDescriptor.exp, '');
  return false;
 }

function ncpInstance ()
 {
  try { var ncp = require('ncp').ncp; }
  catch(e)
   {
    console.log('The "ncp" module is not installed [sudo npm install ncp]');
    process.exit(0);
   };
  ncp.limit = 16;
  return ncp;
 }

function normalizeDestinationPath (destination)
 {
  var isFull = !!(destination.match(/^[a-z]\:(\\|\/)/ig) || destination.match(/^(\\|\/)/g));
  var path = destination;
  if (!isFull)
   {
    if (typeof __arguments.destination == 'string' && typeof process.env.PWD != 'string')
     {
      console.log('Please enter absolute path to destination directory');
      process.exit(0);
     }
    path = (typeof __arguments.destination == 'string' ? process.env.PWD : CONFIG.server.application.dir) + '/' + destination;
   }
  if (!FS.existsSync(convert(path)))
   {
    console.log('Destination directory "'+path+'" is not exist');
    process.exit(0);
   }
  return convert(path, true);
 }

function isPathExists (array, path) // Local path relative to the application directory
 {
  if (array.indexOf(path) >= 0) return true;
  for (var i=0; i<array.length; i++) 
   {
    if (path.indexOf(array[i]) == 0 && path.substr(array[i].length)[0].match(/(\\|\/)/)) return true;
   }
  return false;
 }

function convert (path, normalize)
 {
  function fn (path, normalize)
   {
    if (typeof path != 'string') return path;
    var o = path.replace(/[\\\/]+/g, PATH.sep);
    return !!normalize ? PATH.normalize(o) : o;
   }

  if (path != null && typeof path == 'object')
   {
    for (var i in path) path[i] = fn(path[i], normalize);
    return path;
   }
  else return fn(path, normalize);
 }
function parseString (s)
 {
  var o = {"true":true, "false":false, "null":null, "undefined":undefined, "NaN":NaN};
  for(var i in o)
   {
    if (s == i)
     {
      return o[i];
     }
   }
  if(/^[\.0-9]+$/mg.test(s))
   {
    return Number(s);
   }
  return s;
 };
module.exports.parseString = parseString;
 
function parseArguments (array, separator)
 {
  var a = (typeof array == "object" && array.length) ? array : [];
  var s = (typeof separator == "string" && separator.length) ? separator : '=';

  var o = {};
  for (var i=0; i<a.length; i++)
   {
    var v = a[i].split(s);
    o[v[0]] = (typeof v[1] == 'string') ? parseString(v[1]) : null;
   }

  return o;
 };
module.exports.parseArguments = parseArguments;

function getContentTypeByExt (ext, alternate)
 {
  var result = "application/octet-stream";
  var o = {
    "txt": "text/plain",
    "htm": "text/html",
    "html": "text/html",
    "php": "text/html",
    "css": "text/css",
    "js": "application/javascript",
    "json": "application/json",
    "xml": "application/xml",
    "swf": "application/x-shockwave-flash",
    "flv": "video/x-flv",
    "png": "image/png",
    "jpe": "image/jpeg",
    "jpeg": "image/jpeg",
    "jpg": "image/jpeg",
    "gif": "image/gif",
    "bmp": "image/bmp",
    "ico": "image/vnd.microsoft.icon",
    "tiff": "image/tiff",
    "tif": "image/tiff",
    "svg": "image/svg+xml",
    "svgz": "image/svg+xml",
    "zip": "application/zip",
    "rar": "application/x-rar-compressed",
    "exe": "application/x-msdownload",
    "msi": "application/x-msdownload",
    "cab": "application/vnd.ms-cab-compressed",
    "mp3": "audio/mpeg",
    "qt": "video/quicktime",
    "mov": "video/quicktime",
    "pdf": "application/pdf",
    "psd": "image/vnd.adobe.photoshop",
    "ai": "application/postscript",
    "eps": "application/postscript",
    "ps": "application/postscript",
    "doc": "application/msword",
    "rtf": "application/rtf",
    "xls": "application/vnd.ms-excel",
    "ppt": "application/vnd.ms-powerpoint",
    "odt": "application/vnd.oasis.opendocument.text",
    "ods": "application/vnd.oasis.opendocument.spreadsheet",
    "woff": "application/font-woff",
    "ttf": "application/x-font-ttf",
    "eot": "application/vnd.ms-fontobject"
  };
  var ext = String(ext).toLowerCase();
  if(o[ext] != null) result = o[ext];
  else if (alternate != null) result = (o[alternate] != null) ? o[alternate] : alternate;
  return result;
 }
module.exports.getContentTypeByExt = getContentTypeByExt;

function getContentTypeByFilename (filename, alternate)
 {
  var m = filename.replace(/\?.*$/mg, '').split(/[\\/]/).pop().match(/(.*)\.([^.]+)$/);
  return getContentTypeByExt (m==null?'':m[2], alternate);
 }
module.exports.getContentTypeByFilename = getContentTypeByFilename;

function getObjectPropertyByPath (path, object)
 {
  var request = (typeof path == 'string' && path.length > 0) ? path : null;
  if(!request) return false;
  var parent = null;
  var target = object;
  var path = request.split('.');
  for (var i=0; i<path.length; i++)
   {
    if
     (
      path[i].toLowerCase() == 'top'
      || path[i].toLowerCase() == 'parent'
      || path[i].toLowerCase() == '__proto__'
      || typeof target == 'undefined'
      || typeof path[i] == 'undefined'
     ) return false;

    parent = target;
    target = target[path[i]];
    if
     (
      typeof parent == 'undefined'
      || typeof target == 'undefined'
      || parent.toString().indexOf('[native code]') > -1
      || target.toString().indexOf('[native code]') > -1
     ) return false;
   }
  if (!parent || typeof target == 'undefined') return false;
  return { 'context':parent, 'target':target };
};
module.exports.getObjectPropertyByPath = getObjectPropertyByPath;

function allowCrossDomain (req, res, next)
 {
  if (req.headers.origin) res.header('Access-Control-Allow-Origin', req.headers.origin);
  // res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE');
  // res.header('Access-Control-Allow-Headers', 'Origin,X-Custom-Header,X-Requested-With,Content-Type,Accept'); 

  // intercept OPTIONS method
  if ('OPTIONS' == req.method)
   {
    res.send(200);
    return;
   }

  next();
 };
module.exports.allowCrossDomain = allowCrossDomain;

function extend (a, b)
 {
  for(var p in b)
   {
    try
     {
      if(typeof b[p] == 'object' && b[p] != null) a[p] = extend(a[p], b[p]);
      else a[p] = b[p];
     }
    catch(e)
     {
      a[p] = b[p];
     };
   }
  return a;
 };
module.exports.extend = extend;

/**
 * Server Tag processing
 *
 * @param express (object) Express instance
 * @param app (object) Express app instance
 * @param route (string|array) Route definition string
 * @param tagObjectFilePath (string) Path to file that provides route specific tag-processor object
 * @param staticFilePath (string|function) Path to static file that should to be processed
 */
function addTagBasedRoute (express, app, route, tagObjectFile, staticFile)
 {
  var builder = require(__dirname + '/builder.js');
  var fs = require('fs');

  function init (route, tagObjectFile, staticFile)
   {
    app.all(route, function(req, res)
     {
      var filepath = (typeof staticFile == 'function') ? staticFile(req, res) : staticFile;
      var path = builder.path(filepath);
      if ( typeof filepath != 'string' || !fs.existsSync(filepath) )
       {
        res.send(404);
        return false;
       }
      var str = fs.readFileSync(path,'utf-8');
      if (path != filepath) res.send(str);
      else
       {
        var result = new require(tagObjectFile)(str, req, res);
        res.send(result);        
       }
     });
   }
  var routes = (route instanceof Array) ? route : [route];
  for (var i=0; i<routes.length; i++)
   {
    init(routes[i], tagObjectFile, staticFile);
   }
 }
module.exports.addTagBasedRoute = addTagBasedRoute;

/**
 * Server requests processing
 *
 * @param express (object) Express instance
 * @param app (object) Express app instance
 * @param route (string|array) Route definition string
 * @param filepath (string|function) Path to the file that should to process server request
 */
function addServerRequestRoute (express, app, route, filepath)
 {
  function init (route, filepath)
   {
    var _path = filepath; // Fucking NodeJS closure fix
    app.all(route, function(req, res)
     {
      var filepath = (typeof _path == 'function') ? _path(req, res) : _path;
      if(typeof filepath != 'string' || !require('fs').existsSync(filepath))
       {
        res.send(404);
        return false;
       }
      require(filepath)(req, res);
      return;
     });
   }
  var routes = (route instanceof Array) ? route : [route];
  for (var i=0; i<routes.length; i++)
   {
    init(routes[i], filepath);
   }
 }
module.exports.addServerRequestRoute = addServerRequestRoute;

var UglifyJS = null;
try {UglifyJS = require('uglify-js');} catch(e){
 console.warn('WARNING: An "uglify-js" module is not installed.\nRockJS requires this module for production version [sudo npm install uglify-js]');
};

function canMinifyJS ()
 {
  return !(UglifyJS == null);
 }
module.exports.canMinifyJS = canMinifyJS;

function minifyJS (js)
 {
  var o = {fromString: true, compress:{hoist_funs:false}};
  return ( canMinifyJS() ) ? (UglifyJS.minify(js, o)).code : js;
 }
module.exports.minifyJS = minifyJS;

function minifyCSS (css)
 {
  return String(css)
   .replace(/\/\*[\s\S]*?\*\//g, ' ') // Comments
   .replace(/\s+/g, ' ') // Extra spaces
   .replace(/([\(\)\{\}\:\;\,]) /g, '$1') // Extra spaces
   .replace(/ \{/g, '{') // Extra spaces
   .replace(/\;\}/g, '}') // Last semicolon
   .replace(/ ([+~>]) /g, '$1') // Extra spaces
   .replace(/([^{][,: \(\)]0)(%|px|pt|pc|rem|em|ex|cm|mm|in)([, };\(\)])/g, '$1$3') // Units for zero values
   .replace(/([: ,=\-\(])0\.(\d)/g, '$1.$2') // Lead zero for float values
   .replace(/([^\}]*\{\s*?\})/g, '') // Empty rules
   .replace(/([,: \(])#([0-9a-f]{6})/gi, function(m, pfx, clr) { // HEX code reducing
    if (clr[0] == clr[1] && clr[2] == clr[3] && clr[4] == clr[5]) return pfx + '#' + clr[0] + clr[2] + clr[4];
    return pfx + '#' + clr;
   })
   .replace(/(margin|padding|border-width|border-color|border-style)\:([^;}]+)/gi, function(m,k,v){
    function chk ()
     {
      var o = arguments.length > 1 ? arguments : arguments.length == 1 ? arguments[0] : [];
      for (var i=0; i<o.length; i++)
       {
        if (i==0) continue;
        if (o[i] != o[i-1]) return false;
       }
      return true;
     }
     var o = v.toLowerCase().split(' ');
     var r = v;
     if (chk(o)) r = o[0];
     else if ( (o.length == 4 && chk(o[0], o[2]) && chk(o[1],o[3])) || (o.length == 3 && chk(o[0],o[2])) ) r = o[0] + ' ' + o[1];
     else if (o.length == 4 && chk(o[1],o[3])) r = o[0] + ' ' + o[1] + ' ' + o[2];
     r = k + ':' + r;
     return r;
   });
 }
module.exports.minifyCSS = minifyCSS;

function minifyHTML (html)
 {
  function mask (id) {return '$@'+id+'@$';}

  var scripts = [];

  var html = String(html)
   .replace(/(<script\b[^>]*>)([\s\S]*?)(<\/script>)/img, function(tag, open, value, close){
    if (canMinifyJS()) return open + minifyJS(value) + close;
    else
     {
      var id = scripts.length;
      scripts[id] = tag;
      return mask(id);      
     }
   })
   .replace(/(<style\b[^>]*>)([\s\S]*?)(<\/style>)/img, function(tag, open, value, close){
    return open + minifyCSS(value) + close;
   })
   .replace(/<!--(?!\s*(?:\[if [^\]]+]|<!|>))(?:(?!-->)(.|\n))*-->/g,"")
   .replace(/\s+/g, ' ')
   .replace(/>\s+</g, '><')
  ;
  if (!canMinifyJS())
   {
    for (var i=0; i<scripts.length; i++) html = html.replace(mask(i), scripts[i]);    
   }
  return html;
 }
module.exports.minifyHTML = minifyHTML;

function pregQuote (string)
 {
  return String(string).replace(/([\\\.\+\*\?\[\^\]\$\(\)\{\}\=\!\<\>\|\:])/g, "\\$1");
 }
module.exports.pregQuote = pregQuote;

function fileCopy (from, to, callback)
 {
  var fs = require('fs');
  var _done = false;

  var r = fs.createReadStream(from);
  r.on("error", done);

  var w = fs.createWriteStream(to, {flags:'w+'});
  w.on("error", done);
  w.on("close", function(ex){done();});
  r.pipe(w);

  function done(err)
   {
    if (!_done)
     {
      _done = true;
      if (typeof callback == 'function') callback(err);
     }
   }
 }
module.exports.fileCopy = fileCopy;
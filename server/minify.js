var helper = require(__dirname + '/helpers.js');
var config = require(__dirname + '/../config/config.js');

module.exports.js = function js (string)
 {
  return !config.generic.debug.status ? helper.minifyJS(string) : string;
 };

module.exports.css = function css (string)
 {
  return !config.generic.debug.status || config.generic.debug.minify ? helper.minifyCSS(string) : string;
 };

module.exports.html = function html (string)
 {
  return !config.generic.debug.status || config.generic.debug.minify ? helper.minifyHTML(string) : string;
 };
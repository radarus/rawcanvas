module.exports = function router (express, app)
 {

  var helper = require(__dirname + '/helpers.js');
  var config = require(__dirname + '/../config/config.js');

  /**
   * {config.generic.modules.url}/{MODULE_NAME}
   * {config.generic.modules.url}/{MODULE_NAME}/server.js
   */
  helper.addServerRequestRoute
   (
    express, app,
    [config.generic.modules.url + '/:name', config.generic.modules.url + '/:name/server.js'],
    function(req, res)
     {
      if ( !(/^[a-z0-9_-]+$/ig).test(req.params.name) ) return false;
      return config.server.modules.dir + '/' + req.params.name + '/server.js';
     }
   );


  /**
   * Tag based routes
   */

  function addCOMTagBasedRoutes (dir, url)
   {
    /**
     * {dir}/{COM_NAME}/client.js  => {url}/{COM_NAME}/client.js
     * {dir}/{COM_NAME}/style.css  => {url}/{COM_NAME}/style.css
     * {dir}/{COM_NAME}/index.html => {url}/{COM_NAME}/index.html
     */
    var files = ['client.js', 'style.css', 'index.html'];

    function make (dir, url, filename)
     {
      helper.addTagBasedRoute(express, app, url + '/:name/' + filename, './tag.generic.js', function(req, res){
       if ( !(/^[a-z0-9_-]+$/ig).test(req.params.name) ) return false;
       return (dir + '/' + req.params.name + '/' + filename);
      });
     }

    for (var i=0; i<files.length; i++)
     {
      make(dir, url, files[i]);
     }
   }

  /**
   * Modules
   */
  addCOMTagBasedRoutes(config.server.modules.dir, config.generic.modules.url);

  /**
   * Layouts
   */
  addCOMTagBasedRoutes(config.server.layouts.dir, config.generic.layouts.url);

  /**
   * core/js/bootstrap.js
   */
  helper.addTagBasedRoute(express, app, '/bootstrap.js', './tag.generic.js', config.server.framework.dir + '/core/js/bootstrap.js');

  /**
   * core/js/generic/com.js
   */
  helper.addTagBasedRoute(express, app, '/core/js/generic/com.js', './tag.generic.js', config.server.framework.dir + '/core/js/generic/com.js');

  /**
   * index.html
   */
  helper.addTagBasedRoute(express, app, ['/', '/index.html'], './tag.generic.js', config.server.framework.dir + '/index.html');

 };
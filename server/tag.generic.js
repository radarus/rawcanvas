module.exports = function (content)
 {
  var TagProcessor = require(__dirname + '/tag.processor.js');
  var bundle = require(__dirname + '/bundle.js');
  var config_js = __dirname + '/../config/config.js';
  var config = require(config_js);

  var that = {};

  /**
   * [require src="{(string) filename}" namespace="{(string) FRAMEWORK}" format="{(string) JSON|BASE64}" default="{(mixed)}"]
   */
  that.require = function (attr)
   {
    var dir = (attr.namespace == 'FRAMEWORK') ? config.server.framework.dir : config.server.application.dir;
    var path = dir + '/' + attr.src;
    //console.log(path)
    var fs = require('fs');
    var minify = require(__dirname + '/minify.js');
    var str = '';

    var exists = fs.existsSync(path);
    var ignore = !bundle.chk(path);

    if (!exists)
     {
      console.log('ERROR: Required file "'+path+'" not found!');
     }

    if (!exists || ignore) str = (typeof attr['default'] != 'undefined') ? attr['default'] : '';
    else
     {
      str = fs.readFileSync(path, (attr.format == 'BASE64') ? 'base64' : 'utf-8');
      if (attr.format != 'BASE64') str = TagProcessor(str, that);
     }

    if (attr.src.match(/\.(html|htm)$/i)) str = minify.html(str);
    else if (attr.src.match(/\.(css)$/i)) str = minify.css(str);
    else if (attr.src.match(/\.(js)$/i)) str = minify.js(str);

    if (attr.format && attr.format == 'JSON') str = JSON.stringify(str);
    return str;
   };

  /**
   * [variable name="{(string) varname}"]
   * Available variable names:
   * + config
   */
  that.variable = function (attr)
   {

    function getNodeModuleProperty (path, file)
     {
      var result = null;
      if (path.length && file.length)
       {
        var getObjectPropertyByPath = require('./helpers.js').getObjectPropertyByPath;
        var context = require(file);
        result = getObjectPropertyByPath(path, context);
       }
      return (result != null && typeof result == 'object') ? result.target : undefined;
     }

    if (attr.name == 'config')
     {
      var o = {
        generic: config.generic,
        client: config.client
      };
      return JSON.stringify(o);
     }

    if
     (
      attr.name.indexOf('config.generic.') === 0 ||
      attr.name.indexOf('config.client.') === 0
     )
     {
      var prefix = 'config.';
      var path = attr.name.substr(prefix.length);
      return JSON.stringify( getNodeModuleProperty(path, config_js) );
     }

    return undefined;
   };

  /**
   * Client document.URL
   * [URL]
   */
  that.URL = function ()
   {
    return config.client.url;
   };

  return TagProcessor(content, that);
 };
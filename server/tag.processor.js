module.exports = function (content, provider)
 {

  function mapString (string, fn)
   {
    if(typeof fn != 'function')
     {
      return false;
     }
    var f = fn, s = String(string);
    function opts (s)
     {
      var a = {};
      var e = /([a-zA-z0-9-_:]+)=(["'])(.*?)\2/g;
      s.replace(e, function(){
       a[arguments[1]] = arguments[3];
       return '';
      });
      return a;
     }
    var e = /\<\!\-\-\[([a-zA-z0-9-_:]+)\s*(.*?)\]\-\-\>|\/\*\[([a-zA-z0-9-_:]+)\s*(.*?)\]\*\//g;
    return s.replace(e, function(){
     return f(arguments[1] || arguments[3], opts(arguments[2] || arguments[4]), arguments[0]);
    });
   }

  function tagProcessing (name, attr, string)
   {
    if (typeof provider[name] == 'function')
     {
      return provider[name](attr);
     }
   }

  return mapString(content, tagProcessing);
 };